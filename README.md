# Chjixel

Chjixel is a simple multiplayer canvas. Players are anonymous and can place one
pixel on a canvas with a fixed time delay.

## How to run

- Run `npm install`
- Run `NODE_ENV=production npm run build` (alternatively, `npm run watch`)
- Copy and modify `example.json5` to your taste
- Run `NODE_ENV=production npm start <your config.json5>`

## API

### Tokens

To enforce a timeout, each player session is identified by a token. A token is
16 alphanumeric characters, matching the regex `[0-9a-zA-Z]{16}`.

A certain number of tokens per IP address is allowed, by default 10, above which
no new token will be accepted until at least one has its timer expire.

### Index and color

Positions within the canvas are identified by their index. The index is simply
the Y position, multiplied by the canvas width, and with the X position added.

The color palette has 27 colors, identified by 3-level RGB (values 0, 1, 2). The
color index is produced with `r * 9 + g * 3 + b * 1`.

The 24-bit RGB values are taken by multiplying each component with 127 (this
means the levels are 0, 127, 254 - note, not 255).

### `GET /canvas.png`

Sends a PNG image of the entire canvas.

### `GET /b/dim`

This endpoint returns a JSON object containing the canvas width and height
inside its `width` and `height` attributes.

### `GET /b/events`

`/b/events` is the server-sent events (SSE) endpoints used to receive real-time
canvas updates. It takes two optional query string arguments: `token`, the token
if the session listens for timer events on a token, and `events`, a negative
mask of which events to not send.

The events sent are:

- `canvas` (except if `events` contains `c`) - Sent when the connection is
  established, contains a base64'd PNG of the entire canvas.
- `place` (except if `events` contains `p`) - Sent whenever a new pixel is
  placed. Contains a JSON object with the attributes `i` (the position index)
  and `color` (the color index).
- `timer` (only if `token` set, except if `events` contains `t`) - Sent when the
  connection is established and if the token has an outstanding timer, returns
  its current value in a JSON object with the attribute `timer`.
- `ready` (only if `token` set, except if `events` contains `t`) - Sent when the
  timer associated with the token expires.

### `PUT /b/place`

`/b/place` is the endpoint used to place pixels. It takes as body a JSON object
containing the attributes `token`, the token placing the pixel, `i`, the
position index, and `color`, the color index.

The return status is 200 if it succeeded, in which case the body will be either
an empty JSON object if no timeout is set in the config, or a JSON object with
the `timer` attribute containing the timer value.

If a pixel of the same color already exists, the status is 409, with an empty
JSON object.

If the token has an outstanding timer, the status is 429, with a JSON object
containing the attribute `timer` with the outstanding timer value. If there is
no `timer` attribute, then it means there are too many active tokens on the IP
address.
