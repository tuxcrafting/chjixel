import * as JSON5 from 'json5';
import { readFile, mkdir } from 'node:fs/promises';
import { Config } from './config';
import { App } from './app';

(async () => {
    process.on('SIGINT', () => { process.exit(0); });
    process.on('SIGHUP', () => { process.exit(1); });
    process.on('SIGTERM', () => { process.exit(1); });

    if (process.argv.length !== 3) {
        console.error('Please specify configuration file.');
        process.exit(1);
    }

    const config = new Config(JSON5.parse(await readFile(process.argv[2], { encoding: 'utf-8' })));

    await mkdir(config.schema.datadir, {
        recursive: true,
    });

    const app = new App(config);
    process.on('exit', async () => { await app.close(); })

    app.exp.listen(config.port, () => {
        console.log(`listening on http://localhost:${config.port}`);
    });
})();
