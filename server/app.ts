import { Channel, createChannel, createSession } from 'better-sse';
import * as express from 'express';
import { Application, Request, Response } from 'express';
import { Canvas } from './canvas';
import { Config } from './config';
import { SessionManager } from './sessman';
import { paletteSize } from './palette';

function validateToken(token: string): boolean {
    return /^[0-9a-zA-Z]{16}$/.test(token);
}

export class App {
    config: Config;
    canvas: Canvas;
    sm: SessionManager;
    exp: Application;
    placeChannel: Channel = createChannel();

    constructor(config: Config) {
        this.config = config;

        this.canvas = new Canvas(this);
        this.sm = new SessionManager(this);

        this.exp = express();
        this.exp.set('trust proxy', true);

        this.exp.use(express.static('./dist', {
            fallthrough: true,
        }));

        this.exp.get('/canvas.png', (_, res) => {
            res.type('png');
            res.end(this.canvas.dataPNG, 'binary');
        });

        this.exp.get('/b/dim', (_, res) => {
            res.json({ width: this.config.width, height: this.config.height });
        });

        this.exp.get('/b/events', this.handleEvents.bind(this));

        this.exp.use('/b/place', express.json());
        this.exp.put('/b/place', this.handlePlace.bind(this));
    }

    async handleEvents(req: Request, res: Response): Promise<void> {
        res.header('X-Accel-Buffering', 'no');

        const token = typeof req.query.token === 'string' ? req.query.token : undefined;
        const events = typeof req.query.events === 'string' ? req.query.events : '';

        if (token !== undefined && !validateToken(token)) {
            res.status(400);
            res.end();
            return;
        }

        const session = await createSession(req, res, {
            serializer: (x: string) => x,
        });

        if (events.indexOf('p') === -1) {
            this.placeChannel.register(session);
        }

        if (events.indexOf('c') === -1) {
            session.push(this.canvas.dataPNG.toString('base64'), 'canvas');
        }

        if (this.config.timeout !== 0 &&
            events.indexOf('t') === -1 &&
            token !== undefined) {
            this.sm.registerSession(token, session);
            const t = this.sm.getTimer(token);
            if (t !== null)
                session.push(JSON.stringify({ timer: t }), 'timer');
        }
    }

    handlePlace(req: Request, res: Response): void {
        if (typeof req.body !== 'object' ||
            typeof req.body.token !== 'string' ||
            typeof req.body.i !== 'number' ||
            typeof req.body.color !== 'number' ||
            !validateToken(req.body.token) ||
            Math.floor(req.body.i) !== req.body.i ||
            Math.floor(req.body.color) !== req.body.color ||
            req.body.i < 0 || req.body.i >= this.config.canvasSize ||
            req.body.color < 0 || req.body.color >= paletteSize) {
            res.status(400);
            res.end();
            return;
        }

        const { token, i, color }: { token: string, i: number, color: number } = req.body;

        const date = new Date();

        if (this.config.timeout !== 0) {
            const timer = this.sm.getTimer(token);
            if (timer !== null) {
                res.status(429);
                res.json({ timer: timer });
                return;
            }

            if (!this.sm.checkIP(req.ip)) {
                res.status(429);
                res.json({});
                return;
            }
        }

        if (!this.canvas.place(date, i, color)) {
            res.status(409);
            res.json({});
            return;
        }

        this.placeChannel.broadcast(JSON.stringify({i, color}), 'place');

        if (this.config.timeout !== 0) {
            this.sm.setTimer(token, req.ip, date);
            res.json({ timer: this.config.timeout });
        } else {
            res.json({});
        }
    }

    async close(): Promise<void> {
        await this.canvas.close();
    }
}
