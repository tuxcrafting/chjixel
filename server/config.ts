import Ajv from 'ajv';
import { join } from 'node:path';

interface ConfigSchema {
    port: number;
    datadir: string;
    maxSessionsPerIP: number;
    timeout: number;
    width: number;
    height: number;
}

const validateConfigSchema = new Ajv({
    allErrors: true,
}).compile<ConfigSchema>({
    type: 'object',
    required: ['port', 'datadir', 'maxSessionsPerIP', 'timeout', 'width', 'height'],
    additionalProperties: false,
    properties: {
        port: {
            type: 'integer',
            minimum: 0,
            maximum: 65535,
        },
        datadir: {
            type: 'string',
        },
        maxSessionsPerIP: {
            type: 'integer',
            minimum: 0,
        },
        timeout: {
            type: 'integer',
            minimum: 0,
        },
        width: {
            type: 'integer',
            minimum: 1,
        },
        height: {
            type: 'integer',
            minimum: 1,
        },
    },
});

export class Config {
    schema: ConfigSchema;
    port: number;
    maxSessionsPerIP: number;
    timeout: number;
    width: number;
    height: number;
    canvasSize: number;

    constructor(data: any) {
        if (!validateConfigSchema(data))
            throw new SyntaxError(
                validateConfigSchema.errors!.map((err) =>
                    `${err.instancePath}: ${err.message}`).join('\n'));

        this.schema = data;
        this.port = data.port;
        this.maxSessionsPerIP = data.maxSessionsPerIP;
        this.timeout = data.timeout;
        this.width = data.width;
        this.height = data.height;
        this.canvasSize = this.width * this.height;
    }

    datadir(name: string): string {
        return join(this.schema.datadir, name);
    }

    toXY(i: number): [number, number] {
        const x = i % this.width;
        const y = Math.floor(i / this.width);
        return [x, y];
    }

    fromXY(x: number, y: number): number {
        return y * this.width + x;
    }
}
