import { Session } from 'better-sse';
import { App } from './app';
import { ZMap } from './zmap';

function setZMap<KeyType, ItemType>(): ZMap<KeyType, Set<ItemType>> {
    return new ZMap(
        () => new Set,
        (s) => s.size === 0);
}

export class SessionManager {
    app: App;
    tokenTimeout: Map<string, Date> = new Map;
    tokenSessions: ZMap<string, Set<Session>> = setZMap();
    ipTokens: ZMap<string, Set<string>> = setZMap();

    constructor(app: App) {
        this.app = app;
    }

    registerSession(token: string, session: Session): void {
        const s = this.tokenSessions.get(token);
        s.add(session);
        this.tokenSessions.set(token, s);

        session.on('disconnected', () => {
            const s = this.tokenSessions.get(token);
            s.delete(session);
            this.tokenSessions.set(token, s);
        });
    }

    checkIP(ip: string): boolean {
        const ips = this.ipTokens.get(ip);
        if (this.app.config.maxSessionsPerIP !== 0 &&
            ips.size === this.app.config.maxSessionsPerIP) {
            return false;
        }
        return true;
    }

    setTimer(token: string, ip: string, date: Date): void {
        {
            const ips = this.ipTokens.get(ip);
            ips.add(token);
            this.ipTokens.set(ip, ips);
        }

        this.tokenTimeout.set(token, date);
        setTimeout(() => {
            this.tokenTimeout.delete(token);
            const ips = this.ipTokens.get(ip);
            ips.delete(token);
            this.ipTokens.set(ip, ips);

            for (const session of this.tokenSessions.get(token))
                session.push('', 'ready');
        }, this.app.config.timeout * 1000);
    }

    getTimer(token: string): number | null {
        const t = this.tokenTimeout.get(token);
        if (t === undefined)
            return null;
        return Math.round(this.app.config.timeout - (new Date().getTime() - t.getTime()) / 1000);
    }
}
