type CreateFn<KeyType, ValueType> = (k: KeyType) => ValueType;
type DeleteFn<KeyType, ValueType> = (v: ValueType, k: KeyType) => boolean;

export class ZMap<KeyType, ValueType> {
    createFn: CreateFn<KeyType, ValueType>;
    deleteFn: DeleteFn<KeyType, ValueType>;
    map: Map<KeyType, ValueType> = new Map;

    constructor(createFn: CreateFn<KeyType, ValueType>, deleteFn: DeleteFn<KeyType, ValueType>) {
        this.createFn = createFn;
        this.deleteFn = deleteFn;
    }

    get(k: KeyType): ValueType {
        if (this.map.has(k))
            return this.map.get(k)!;
        const t = this.createFn(k);
        this.map.set(k, t);
        return t;
    }

    set(k: KeyType, v: ValueType): void {
        if (this.deleteFn(v, k))
            this.map.delete(k);
        else
            this.map.set(k, v);
    }
}
