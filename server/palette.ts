export const paletteSize = 3 * 3 * 3;

export function index_2_rgb3(i: number): [number, number, number] {
    const r = Math.floor(i / 9) % 3;
    const g = Math.floor(i / 3) % 3;
    const b = Math.floor(i / 1) % 3;
    return [r, g, b];
}

export function index_2_rgb24(i: number): [number, number, number] {
    const r = Math.floor(i / 9) % 3;
    const g = Math.floor(i / 3) % 3;
    const b = Math.floor(i / 1) % 3;
    return [r * 127, g * 127, b * 127];
}

export function rgb3_2_index(r: number, g: number, b: number): number {
    return r * 9 + g * 3 + b * 1;
}

export function rgb24_2_index(r: number, g: number, b: number): number {
    return rgb3_2_index(
        Math.round(r / 127),
        Math.round(r / 127),
        Math.round(r / 127));
}

export function rgb3_2_rgb24(r: number, g: number, b: number) {
    return [r * 127, g * 127, b * 127];
}

export function rgb24_2_rgb3(r: number, g: number, b: number) {
    return [r / 127, g / 127, b / 127].map(Math.round);
}
