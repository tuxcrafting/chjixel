import { Database, RootDatabase, open } from 'lmdb';
import { PNG } from 'pngjs';
import { App } from './app';
import { index_2_rgb24 } from './palette';

interface LogEntry {
    date: string;
    i: number;
    color: number;
}

export class Canvas {
    app: App;
    db: RootDatabase<string, string>;
    dbCanvas: Database<number, number>;
    dbLog: Database<LogEntry, string>;
    latestIndex: bigint;

    imagePNG: PNG;
    dataPNG: Buffer;

    constructor(app: App) {
        this.app = app;

        this.db = open({
            path: this.app.config.datadir('db'),
            compression: true,
        });

        this.dbCanvas = this.db.openDB({
            name: 'canvas',
        });

        this.dbLog = this.db.openDB({
            name: 'log',
        });

        {
            const t = this.db.get('latestIndex');
            if (t === undefined)
                this.db.putSync('latestIndex', (this.latestIndex = 0n).toString());
            else
                this.latestIndex = BigInt(t);
        }

        this.imagePNG = new PNG({
            width: this.app.config.width,
            height: this.app.config.height,
            colorType: 6,
            bitDepth: 8,
        });

        for (let i = 0; i < this.app.config.canvasSize; i++) {
            const k = this.dbCanvas.get(i) || 26;
            const [r, g, b] = index_2_rgb24(k);
            this.imagePNG.data[i * 4 + 0] = r;
            this.imagePNG.data[i * 4 + 1] = g;
            this.imagePNG.data[i * 4 + 2] = b;
            this.imagePNG.data[i * 4 + 3] = 255;
        }

        this.updatePNG();
    }

    async close(): Promise<void> {
        await this.db.close();
    }

    updatePNG(): void {
        this.dataPNG = PNG.sync.write(this.imagePNG);
    }

    place(date: Date, i: number, color: number): boolean {
        return this.db.transactionSync(() => {
            if ((this.dbCanvas.get(i) || 26) === color)
                return false;

            this.dbCanvas.putSync(i, color);
            this.dbLog.putSync((this.latestIndex++).toString(), {
                date: date.toISOString(),
                i: i,
                color: color,
            });
            this.db.putSync('latestIndex', this.latestIndex.toString());

            const [r, g, b] = index_2_rgb24(color);
            this.imagePNG.data[i * 4 + 0] = r;
            this.imagePNG.data[i * 4 + 1] = g;
            this.imagePNG.data[i * 4 + 2] = b;
            this.updatePNG();

            return true;
        });
    }
}
