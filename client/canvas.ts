import { Buffer } from 'buffer';
import { PNG } from 'pngjs/browser';
import { index_2_rgb24 } from '../server/palette';

export class Canvas {
    canvas: HTMLCanvasElement;
    ctx: CanvasRenderingContext2D;
    data: ImageData;

    constructor(width: number, height: number) {
        this.canvas = document.createElement('canvas');
        this.canvas.width = width;
        this.canvas.height = height;
        this.ctx = this.canvas.getContext('2d')!;
        this.data = this.ctx.createImageData(width, height);
    }

    render(): void {
        this.ctx.putImageData(this.data, 0, 0);
    }

    loadPNG(data: string): void {
        const png = PNG.sync.read(Buffer.from(data, 'base64'));
        for (const [i, x] of png.data.entries())
            this.data.data[i] = x;
        this.render();
    }

    place(i: number, color: number): void {
        const [r, g, b] = index_2_rgb24(color);
        this.data.data[i * 4 + 0] = r;
        this.data.data[i * 4 + 1] = g;
        this.data.data[i * 4 + 2] = b;
        this.data.data[i * 4 + 3] = 255;
        this.render();
    }
}
