const tokenAlphabet = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
const tokenLength = 16;
const ssKey = 'chjixel.token1';

function generateToken(): string {
    let token = '';
    for (let i = 0; i < tokenLength; i++) {
        const j = Math.floor(Math.random() * tokenAlphabet.length);
        token += tokenAlphabet[j];
    }
    return token;
}

export function getToken(): string {
    const token = sessionStorage.getItem(ssKey);
    if (token !== null)
        return token;

    const nt = generateToken();
    sessionStorage.setItem(ssKey, nt);
    return nt;
}
