import { rgb24_2_rgb3, rgb3_2_index } from '../server/palette';
import { Canvas } from './canvas';
import { Display } from './display';
import { getToken } from './token';
import { Toolbar } from './toolbar';

window.addEventListener('DOMContentLoaded', async () => {
    const { width, height } = await (await fetch('/b/dim')).json();
    const canvas = new Canvas(width, height);

    const displayElm = document.querySelector<HTMLCanvasElement>('#display')!;
    const display = new Display(canvas, displayElm);

    window.addEventListener('resize', () => {
        display.resize(window.innerWidth, window.innerHeight);
    });
    display.resize(window.innerWidth, window.innerHeight);

    const toolbar = new Toolbar;
    toolbar.addEventListener('toolChange', () => {
        display.display.style.cursor = {
            'none': 'grab',
            'pick': 'crosshair',
            'place': 'pointer',
        }[toolbar.tool];
    });

    const token = getToken();
    const events = new EventSource(`/b/events?token=${token}`);

    events.addEventListener('canvas', (ev) => {
        canvas.loadPNG(ev.data);
    });

    events.addEventListener('place', (ev) => {
        const { i, color } = JSON.parse(ev.data);
        canvas.place(i, color);
    });

    events.addEventListener('timer', (ev) => {
        const { timer } = JSON.parse(ev.data);
        toolbar.beginTimer(timer);
    });

    events.addEventListener('ready', () => {
        toolbar.endTimer();
    });

    let dragged = false;

    display.display.addEventListener('mousedown', () => {
        dragged = false;
    });

    display.display.addEventListener('mouseup', async (ev) => {
        const oldDragged = dragged;
        dragged = false;
        if (oldDragged)
            return;

        const [cx, cy] = display.screen2canvas(ev.pageX, ev.pageY).map(Math.floor);
        if (cx < 0 || cx >= canvas.canvas.width ||
            cy < 0 || cy >= canvas.canvas.height)
            return;

        if (toolbar.tool === 'pick') {
            const i = cy * canvas.canvas.width + cx;
            const r = canvas.data.data[i * 4 + 0];
            const g = canvas.data.data[i * 4 + 1];
            const b = canvas.data.data[i * 4 + 2];
            const [r1, g1, b1] = rgb24_2_rgb3(r, g, b);
            toolbar.color.r = r1;
            toolbar.color.g = g1;
            toolbar.color.b = b1;
            toolbar.updateDisplay();
        } else if (toolbar.tool === 'place') {
            if (toolbar.timer.innerText !== '-') {
                return;
            }

            const { r, g, b } = toolbar.color;
            const color = rgb3_2_index(r, g, b);
            const res = await fetch('/b/place', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    token: token,
                    i: cy * canvas.canvas.width + cx,
                    color: color,
                }),
            });

            const data = await res.json();
            if (res.status === 429) {
                if (data.timer !== undefined)
                    toolbar.beginTimer(data.timer);
                else
                    alert('too many sessions on the same IP');
            } else if (res.status === 200) {
                if (data.timer !== undefined)
                    toolbar.beginTimer(data.timer);
            }
        }
    });

    const position = document.querySelector<HTMLSpanElement>('#position')!;
    display.display.addEventListener('mousemove', (ev) => {
        const [cx, cy] = display.screen2canvas(ev.pageX, ev.pageY).map(Math.floor);
        position.innerText = `${cx},${cy}`;

        display.cursorX = cx;
        display.cursorY = cy;

        if (ev.buttons & 1) {
            dragged = true;

            display.centerX -= ev.movementX / display.baseScale / display.zoom;
            display.centerY -= ev.movementY / display.baseScale / display.zoom;
        }
    });

    display.display.addEventListener('wheel', (ev) => {
        display.zoom -= ev.deltaY / 70;
        display.zoom = Math.max(0.5, display.zoom);
    });

    function animationFrame() {
        display.render();
        window.requestAnimationFrame(animationFrame);
    }
    animationFrame();
});
