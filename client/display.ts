import { Canvas } from './canvas';

export class Display {
    canvas: Canvas;
    display: HTMLCanvasElement;
    ctx: CanvasRenderingContext2D;

    centerX: number = 0;
    centerY: number = 0;
    zoom: number = 1;
    baseScale: number;
    cursorX: number = -1;
    cursorY: number = -1;

    constructor(canvas: Canvas, display: HTMLCanvasElement) {
        this.canvas = canvas;

        this.display = display;
        this.ctx = this.display.getContext('2d')!;

        const url = new URL(location.href);
        const x = url.searchParams.get('x');
        this.centerX = x === null ? this.canvas.canvas.width / 2 : parseInt(x);
        const y = url.searchParams.get('y');
        this.centerY = y === null ? this.canvas.canvas.height / 2 : parseInt(y);
        const s = url.searchParams.get('s');
        this.zoom = s === null ? 1 : parseFloat(s);

        setInterval(this.updateParams.bind(this), 200);
    }

    updateParams() {
        const url = new URL(location.href);
        url.searchParams.set('x', Math.round(this.centerX).toString());
        url.searchParams.set('y', Math.round(this.centerY).toString());
        url.searchParams.set('s', this.zoom.toFixed(2));
        const t = url.toString();
        if (location.href !== t) {
            history.replaceState('', '', t);
        }
    }

    resize(width: number, height: number): void {
        this.display.width = width;
        this.display.height = height;

        const md = Math.min(this.display.width, this.display.height);
        const mc = Math.max(this.canvas.canvas.width, this.canvas.canvas.height);
        this.baseScale = md / mc;
    }

    canvas2screen(cx: number, cy: number): [number, number] {
        return [
            (cx - this.centerX) * this.baseScale * this.zoom + this.display.width / 2,
            (cy - this.centerY) * this.baseScale * this.zoom + this.display.height / 2,
        ];
    }

    screen2canvas(sx: number, sy: number): [number, number] {
        return [
            (sx - this.display.width / 2) / this.baseScale / this.zoom + this.centerX,
            (sy - this.display.height / 2) / this.baseScale / this.zoom + this.centerY,
        ];
    }

    render(): void {
        this.ctx.fillStyle = 'rgb(100, 100, 100)';
        this.ctx.fillRect(0, 0, this.display.width, this.display.height);

        {
            const [sx1, sy1] = this.canvas2screen(0, 0);
            const [sx2, sy2] = this.canvas2screen(this.canvas.canvas.width, this.canvas.canvas.height);
            this.ctx.imageSmoothingEnabled = false;
            this.ctx.drawImage(this.canvas.canvas, sx1, sy1, sx2 - sx1, sy2 - sy1);
        }

        if (this.cursorX >= 0 && this.cursorX < this.canvas.canvas.width &&
            this.cursorY >= 0 && this.cursorY < this.canvas.canvas.height) {
            const [sx, sy] = this.canvas2screen(this.cursorX, this.cursorY);
            this.ctx.strokeStyle = 'black';
            this.ctx.lineWidth = 1;
            this.ctx.strokeRect(sx, sy, this.baseScale * this.zoom, this.baseScale * this.zoom);
        }
    }
}
