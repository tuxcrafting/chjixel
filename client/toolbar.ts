import { index_2_rgb3, rgb3_2_index, rgb3_2_rgb24, paletteSize } from '../server/palette';

export type Tool = 'none' | 'pick' | 'place';

export class Toolbar extends EventTarget {
    color: { r: number, g: number, b: number } = {
        r: 0,
        g: 0,
        b: 0,
    };

    timer: HTMLSpanElement;
    timeoutF: number | null = null;
    colorDisplay: HTMLTableCellElement;
    selectComponent: Record<'r' | 'g' | 'b', HTMLInputElement[]> = {
        r: new Array(3),
        g: new Array(3),
        b: new Array(3),
    };
    selectValue: HTMLInputElement[] = new Array(paletteSize);

    tool: Tool = 'none';

    constructor() {
        super();

        this.timer = document.querySelector<HTMLSpanElement>('#timer')!;
        this.colorDisplay = document.querySelector<HTMLTableCellElement>('#color-display')!;

        for (const t of ['none', 'pick', 'place'])
            document.querySelector<HTMLInputElement>(`#tool-${t}`)!.addEventListener('change', () => {
                this.tool = t as any;
                this.dispatchEvent(new Event('toolChange'));
            });
        for (const q of ['r', 'g', 'b'])
            for (const v of [0, 1, 2])
                this.initComponent(q as any, v);
        for (let i = 0; i < paletteSize; i++)
            this.initValue(i);
    }

    updateDisplay(): void {
        const [r, g, b] = rgb3_2_rgb24(this.color.r, this.color.g, this.color.b);
        this.colorDisplay.style.background = `rgb(${r} ${g} ${b})`;

        this.selectValue[rgb3_2_index(this.color.r, this.color.g, this.color.b)].checked = true;
        this.selectComponent.r[this.color.r].checked = true;
        this.selectComponent.g[this.color.g].checked = true;
        this.selectComponent.b[this.color.b].checked = true;
    }

    initComponent(q: 'r' | 'g' | 'b', v: number): void {
        const e = document.querySelector<HTMLInputElement>(`#color-${q}${v}`)!;
        this.selectComponent[q][v] = e;
        e.addEventListener('change', () => {
            this.color[q] = v;
            const { r, g, b } = this.color;
            this.updateDisplay();
        });
    }

    initValue(i: number): void {
        const [r, g, b] = index_2_rgb3(i);
        const e = document.querySelector<HTMLInputElement>(`#color-${i}`)!;
        this.selectValue[i] = e;
        e.addEventListener('change', () => {
            this.color.r = r;
            this.color.g = g;
            this.color.b = b;
            this.updateDisplay();
        });
    }

    timerCallback(): void {
        const t = this.timer.innerText;
        if (t !== '-' && t !== '0') {
            this.timer.innerText = `${parseInt(t) - 1}`;
            this.timeoutF = setTimeout(this.timerCallback.bind(this), 1000) as any;
        } else {
            this.timeoutF = null;
        }
    }

    beginTimer(n: number): void {
        if (this.timeoutF !== null)
            return;
        this.timer.innerText = `${n}`;
        this.timeoutF = setTimeout(this.timerCallback.bind(this), 1000) as any;
    }

    endTimer(): void {
        this.timer.innerText = '-';
    }
}
